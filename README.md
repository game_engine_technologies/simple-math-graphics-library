# Simple math graphics library

- support matrix;
- support vectors;
- support operation matrix - vectors;
- support help functions (rect, ray etc);
- support templates;
- support exceptions.
